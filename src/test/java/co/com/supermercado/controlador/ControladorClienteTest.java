/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.com.supermercado.controlador;

import co.com.supermercado.modelo.dao.ClienteDAO;
import co.com.supermercado.modelo.dto.ClienteDTO;
import co.com.supermercado.vista.VistaClientes;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Oscar Bonilla Sánchez
 */
public class ControladorClienteTest {

    @Test
    public void consultarClientesPorProfesion_DeberiaRetornarClientesSegunProfesion() {
        VistaClientes vistaCliente = new VistaClientes();
        ControladorCliente controlador = new ControladorCliente(vistaCliente);

        try {
            List<ClienteDTO> listClientes = controlador.consultarListaCliente(null, "Ingeniero(a)");
            assertEquals(2, listClientes.size());
            assertEquals("Oscar", listClientes.get(0).getPrimerNombre());
            assertEquals("Alvaro", listClientes.get(1).getPrimerNombre());
        } catch (Exception ex) {
            Logger.getLogger(ControladorClienteTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
