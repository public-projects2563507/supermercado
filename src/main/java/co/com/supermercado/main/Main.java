/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.com.supermercado.main;

import co.com.supermercado.controlador.ControladorCliente;
import javax.swing.SwingUtilities;
import co.com.supermercado.vista.VistaClientes;

/**
 *
 * @author Oscar Bonilla Sánchez
 */

public class Main {
    public static void main(String[] args) {
        
        VistaClientes vistaCliente = new VistaClientes();
        ControladorCliente controlador = new ControladorCliente(vistaCliente);

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
            
            
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(VistaClientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        SwingUtilities.invokeLater(() -> {
            vistaCliente.setVisible(true);
            vistaCliente.setLocationRelativeTo(null);
            controlador.llenarProfesiones();
        });
    }
}
