/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.com.supermercado.modelo.dao;

import co.com.supermercado.conexion.Conexion;
import co.com.supermercado.modelo.dto.ClienteDTO;
import co.com.supermercado.modelo.dto.ProfesionDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Oscar Bonilla Sánchez
 */
public class ClienteDAO {
    
    public int addCliente(ClienteDTO clienteDTO) throws Exception{
        
        PreparedStatement pst = null;
        ProfesionDAO profesionDAO = null;
        int resultado = -1;
        int indice = 1;
        String query = "INSERT INTO CLIENTES (NUMERO_DOCUMENTO, PRIMER_NOMBRE, "
                + "SEGUNDO_NOMBRE, PRIMER_APELLIDO, SEGUNDO_APELLIDO, DIRECCION, "
                + "TELEFONO, NUMERO_CARNET, PROFESION_ID_FK ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try {
            
            if(clienteDTO.getProfesion().getProfesionId() == null){
                profesionDAO = new ProfesionDAO();
                ProfesionDTO profesionDTO = profesionDAO.getProfesionPorNombre(clienteDTO.getProfesion().getNombre());
                clienteDTO.setProfesion(profesionDTO);
            }
            
            Conexion.getInstance().establecerConexion();
            pst = Conexion.getInstance().getConnection().prepareStatement(query);
            pst.setString(indice++, clienteDTO.getNumeroDocumento()); 
            pst.setString(indice++, clienteDTO.getPrimerNombre()); 
            pst.setString(indice++, clienteDTO.getSegundoNombre()); 
            pst.setString(indice++, clienteDTO.getPrimerApellido()); 
            pst.setString(indice++, clienteDTO.getSegundoApellido()); 
            pst.setString(indice++, clienteDTO.getDireccion()); 
            pst.setString(indice++, clienteDTO.getTelefono()); 
            pst.setString(indice++, clienteDTO.getNumeroCarnet()); 
            pst.setInt(indice++, clienteDTO.getProfesion().getProfesionId()); 
           
            pst.executeUpdate();
            resultado = 1;
            
        } catch (SQLException ex) {
            throw ex;
        }finally{            
            if(Conexion.getInstance().getConnection() != null)
                Conexion.getInstance().getConnection().close();
            if(pst != null){
                try {
                    pst.close();
                } catch (SQLException ex) {
                    throw ex;
                }
                
            }
        }
        
        return resultado;
    }
    
     public int deleteCliente(ClienteDTO clienteDTO) throws Exception{
        
        PreparedStatement pst = null;
        int resultado = -1;
        int indice = 1;
        String query = "DELETE FROM CLIENTES WHERE NUMERO_DOCUMENTO = ?";
        try {
            Conexion.getInstance().establecerConexion();
            pst = Conexion.getInstance().getConnection().prepareStatement(query);
            pst.setString(indice++, clienteDTO.getNumeroDocumento());
           
            pst.executeUpdate();
            resultado = 1;
            
        } catch (SQLException ex) {
            throw ex;
        }finally{            
            if(Conexion.getInstance().getConnection() != null)
                Conexion.getInstance().getConnection().close();
            if(pst != null){
                try {
                    pst.close();
                } catch (SQLException ex) {
                    throw ex;
                }
                
            }
        }
        
        return resultado;
    }
    
    public List<ClienteDTO> getAllClientes() throws Exception{
        
        
        ClienteDTO clienteDTO = null;
        ProfesionDTO profesionDTO = null;
        List<ClienteDTO> listClientes = new ArrayList <ClienteDTO>();
        PreparedStatement pst = null;
        ResultSet resultSet = null;
        int indice = 1;
        String query = "SELECT C.CLIENTE_ID, C.NUMERO_DOCUMENTO, C.PRIMER_NOMBRE, "
                + "C.SEGUNDO_NOMBRE, C.PRIMER_APELLIDO, C.SEGUNDO_APELLIDO, C.DIRECCION, "
                + "C.TELEFONO, C.NUMERO_CARNET, P.PROFESION_ID, P.CODIGO, P.NOMBRE "
                + "FROM CLIENTES C JOIN PROFESIONES P ON (C.PROFESION_ID_FK = P.PROFESION_ID)";
        try {
            
            Conexion.getInstance().establecerConexion();
            pst = Conexion.getInstance().getConnection().prepareStatement(query);
            
           
            resultSet = pst.executeQuery();
            
            while (resultSet.next()) {
                clienteDTO = new ClienteDTO();
                profesionDTO = new ProfesionDTO();
                clienteDTO.setClienteId(resultSet.getInt(indice++));
                clienteDTO.setNumeroDocumento(resultSet.getString(indice++));
                clienteDTO.setPrimerNombre(resultSet.getString(indice++));
                clienteDTO.setSegundoNombre(resultSet.getString(indice++));
                clienteDTO.setPrimerApellido(resultSet.getString(indice++));
                clienteDTO.setSegundoApellido(resultSet.getString(indice++));
                clienteDTO.setDireccion(resultSet.getString(indice++));
                clienteDTO.setTelefono(resultSet.getString(indice++));
                clienteDTO.setNumeroCarnet(resultSet.getString(indice++));
                
                profesionDTO.setProfesionId(resultSet.getInt(indice++));
                profesionDTO.setCodigo(resultSet.getString(indice++));
                profesionDTO.setNombre(resultSet.getString(indice++));
                
                clienteDTO.setProfesion(profesionDTO);
                
                listClientes.add(clienteDTO);
            }
            
        } catch (SQLException ex) {
            throw ex;
        }finally{            
            if(Conexion.getInstance().getConnection() != null)
                Conexion.getInstance().getConnection().close();
            if(pst != null){
                try {
                    pst.close();
                } catch (SQLException ex) {
                    throw ex;
                }
                
            }
                
            if(resultSet != null){
                
                 try {
                    resultSet.close();
                } catch (SQLException ex) {
                    throw ex;
                }
            }
        }
        
        return listClientes;
    }
    
    public List<ClienteDTO> getClientesPorProfesion(String nombre) throws Exception{
        
        
        ClienteDTO clienteDTO = null;
        ProfesionDTO profesionDTO = null;
        List<ClienteDTO> listClientes = new LinkedList <ClienteDTO>();
        PreparedStatement pst = null;
        ResultSet resultSet = null;
        int indice = 1;
        String query = "SELECT C.CLIENTE_ID, C.NUMERO_DOCUMENTO, C.PRIMER_NOMBRE, "
                + "C.SEGUNDO_NOMBRE, C.PRIMER_APELLIDO, C.SEGUNDO_APELLIDO, C.DIRECCION, "
                + "C.TELEFONO, C.NUMERO_CARNET, P.PROFESION_ID, P.CODIGO, P.NOMBRE "
                + "FROM CLIENTES C JOIN PROFESIONES P ON (C.PROFESION_ID_FK = P.PROFESION_ID)"
                + "WHERE P.NOMBRE = ? ORDER BY C.CLIENTE_ID ASC";
        try {
            Conexion.getInstance().establecerConexion();
            pst = Conexion.getInstance().getConnection().prepareStatement(query);
            pst.setString(1, nombre); 
           
            resultSet = pst.executeQuery();
            
            while (resultSet.next()) {
                clienteDTO = new ClienteDTO();
                profesionDTO = new ProfesionDTO();
                indice = 1;
                clienteDTO.setClienteId(resultSet.getInt(indice++));
                clienteDTO.setNumeroDocumento(resultSet.getString(indice++));
                clienteDTO.setPrimerNombre(resultSet.getString(indice++));
                clienteDTO.setSegundoNombre(resultSet.getString(indice++));
                clienteDTO.setPrimerApellido(resultSet.getString(indice++));
                clienteDTO.setSegundoApellido(resultSet.getString(indice++));
                clienteDTO.setDireccion(resultSet.getString(indice++));
                clienteDTO.setTelefono(resultSet.getString(indice++));
                clienteDTO.setNumeroCarnet(resultSet.getString(indice++));
                
                profesionDTO.setProfesionId(resultSet.getInt(indice++));
                profesionDTO.setCodigo(resultSet.getString(indice++));
                profesionDTO.setNombre(resultSet.getString(indice++));
                
                clienteDTO.setProfesion(profesionDTO);
                
                listClientes.add(clienteDTO);
            }
            
        } catch (SQLException ex) {
            throw ex;
        }finally{            
            if(Conexion.getInstance().getConnection() != null)
                Conexion.getInstance().getConnection().close();
            if(pst != null){
                try {
                    pst.close();
                } catch (SQLException ex) {
                    throw ex;
                }
                
            }
                
            if(resultSet != null){
                
                 try {
                    resultSet.close();
                } catch (SQLException ex) {
                    throw ex;
                }
            }
        }
        
        return listClientes;
    }
    
    public ClienteDTO getClientePorCedula(String numeroDocumento) throws Exception{
        
        
        ClienteDTO clienteDTO = null;
        ProfesionDTO profesionDTO = null;
        
        PreparedStatement pst = null;
        ResultSet resultSet = null;
        int indice = 1;
        String query = "SELECT C.CLIENTE_ID, C.NUMERO_DOCUMENTO, C.PRIMER_NOMBRE, "
                + "C.SEGUNDO_NOMBRE, C.PRIMER_APELLIDO, C.SEGUNDO_APELLIDO, C.DIRECCION, "
                + "C.TELEFONO, C.NUMERO_CARNET, P.PROFESION_ID, P.CODIGO, P.NOMBRE "
                + "FROM CLIENTES C JOIN PROFESIONES P ON (C.PROFESION_ID_FK = P.PROFESION_ID)"
                + "WHERE C.NUMERO_DOCUMENTO = ?";
        try {
            Conexion.getInstance().establecerConexion();
            pst = Conexion.getInstance().getConnection().prepareStatement(query);
            pst.setString(1, numeroDocumento); 
           
            resultSet = pst.executeQuery();
            
            while (resultSet.next()) {
                clienteDTO = new ClienteDTO();
                profesionDTO = new ProfesionDTO();
                indice = 1;
                clienteDTO.setClienteId(resultSet.getInt(indice++));
                clienteDTO.setNumeroDocumento(resultSet.getString(indice++));
                clienteDTO.setPrimerNombre(resultSet.getString(indice++));
                clienteDTO.setSegundoNombre(resultSet.getString(indice++));
                clienteDTO.setPrimerApellido(resultSet.getString(indice++));
                clienteDTO.setSegundoApellido(resultSet.getString(indice++));
                clienteDTO.setDireccion(resultSet.getString(indice++));
                clienteDTO.setTelefono(resultSet.getString(indice++));
                clienteDTO.setNumeroCarnet(resultSet.getString(indice++));
                
                profesionDTO.setProfesionId(resultSet.getInt(indice++));
                profesionDTO.setCodigo(resultSet.getString(indice++));
                profesionDTO.setNombre(resultSet.getString(indice++));
                
                clienteDTO.setProfesion(profesionDTO);
            }
            
        } catch (SQLException ex) {
            throw ex;
        }finally{            
            if(Conexion.getInstance().getConnection() != null)
                Conexion.getInstance().getConnection().close();
            if(pst != null){
                try {
                    pst.close();
                } catch (SQLException ex) {
                    throw ex;
                }
                
            }
                
            if(resultSet != null){
                
                 try {
                    resultSet.close();
                } catch (SQLException ex) {
                    throw ex;
                }
            }
        }
        
        return clienteDTO;
    }
    
}
