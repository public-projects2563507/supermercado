/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.com.supermercado.modelo.dao;

import co.com.supermercado.conexion.Conexion;
import co.com.supermercado.modelo.dto.ProfesionDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Oscar Bonilla Sánchez
 */
public class ProfesionDAO {
    
    public List<ProfesionDTO> getAllProfesiones() throws Exception{
        
        ProfesionDTO profesionDTO = null;
        List<ProfesionDTO> listProfesiones = new ArrayList <ProfesionDTO>();
        PreparedStatement pst = null;
        ResultSet resultSet = null;
        String query = "SELECT PROFESION_ID, CODIGO, NOMBRE FROM PROFESIONES";
        
        try {
            Conexion.getInstance().establecerConexion();
            pst = Conexion.getInstance().getConnection().prepareStatement(query);
            
           
            resultSet = pst.executeQuery();
            
            while (resultSet.next()) {
                profesionDTO = new ProfesionDTO();
                profesionDTO.setProfesionId(resultSet.getInt(1));
                profesionDTO.setCodigo(resultSet.getString(2));
                profesionDTO.setNombre(resultSet.getString(3));
                listProfesiones.add(profesionDTO);
            }
            
        } catch (SQLException ex) {
            throw ex;
        }finally{            
            if(Conexion.getInstance().getConnection() != null)
                Conexion.getInstance().getConnection().close();
            if(pst != null){
                try {
                    pst.close();
                } catch (SQLException ex) {
                    throw ex;
                }
                
            }
                
            if(resultSet != null){
                
                 try {
                    resultSet.close();
                } catch (SQLException ex) {
                    throw ex;
                }
            }
        }
        
        return listProfesiones;
    }
    
     public ProfesionDTO getProfesionPorNombre(String nombre) throws Exception{
        
        ProfesionDTO profesionDTO = null;
        PreparedStatement pst = null;
        ResultSet resultSet = null;
        String query = "SELECT PROFESION_ID, CODIGO, NOMBRE FROM PROFESIONES WHERE NOMBRE = ?";
        
        try {
            
            Conexion.getInstance().establecerConexion();            
            pst = Conexion.getInstance().getConnection().prepareStatement(query);
            pst.setString(1, nombre); 
           
            resultSet = pst.executeQuery();
            
            while (resultSet.next()) {
                profesionDTO = new ProfesionDTO();
                profesionDTO.setProfesionId(resultSet.getInt(1));
                profesionDTO.setCodigo(resultSet.getString(2));
                profesionDTO.setNombre(resultSet.getString(3));
                break;
            }
            
        } catch (SQLException ex) {
            throw ex;
        }finally{            
            if(Conexion.getInstance().getConnection() != null)
                Conexion.getInstance().getConnection().close();
            if(pst != null){
                try {
                    pst.close();
                } catch (SQLException ex) {
                    throw ex;
                }
                
            }
                
            if(resultSet != null){
                
                 try {
                    resultSet.close();
                } catch (SQLException ex) {
                    throw ex;
                }
            }
        }
        
        return profesionDTO;
    }
    
  
    
}
