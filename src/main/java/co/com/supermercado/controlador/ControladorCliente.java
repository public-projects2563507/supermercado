/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.com.supermercado.controlador;

import co.com.supermercado.modelo.dao.ClienteDAO;
import co.com.supermercado.modelo.dao.ProfesionDAO;
import co.com.supermercado.modelo.dto.ClienteDTO;
import co.com.supermercado.modelo.dto.ProfesionDTO;
import co.com.supermercado.vista.VistaClientes;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


/**
 *
 * @author Oscar Bonilla Sánchez
 */
public class ControladorCliente {
    

    private VistaClientes vistaCliente = null;
    ProfesionDAO profesionDAO = new ProfesionDAO();
    ClienteDAO clienteDAO = new ClienteDAO();
    
    public ControladorCliente() {
    }
    

    public ControladorCliente(VistaClientes vistaCliente) {
        this.vistaCliente = vistaCliente;
    }
    
    public void llenarProfesiones(){
        
        
        List<ProfesionDTO> listProfesiones = null;
        int indice = 0;
                    
        try {
            listProfesiones = profesionDAO.getAllProfesiones();
            String[] profesiones = new String [listProfesiones.size() + 1];
            profesiones[indice++] = null;
            for(ProfesionDTO profesion : listProfesiones){
                profesiones[indice++] = profesion.getNombre();
            }
            
            vistaCliente.getJcbProfesion1().setModel(new javax.swing.DefaultComboBoxModel<>(profesiones));
            vistaCliente.getJcbProfesion2().setModel(new javax.swing.DefaultComboBoxModel<>(profesiones));
             
        } catch (Exception ex) {
            Logger.getLogger(ControladorCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void registrarCliente(String txtNumeroDocumento, String txtPrimerNombre, String txtSegundoNombre, 
                                    String txtPrimerApellido, String txtSegundoApellido, String txtDireccion,
                                    String txtTelefono, String txtNumeroCarnet, String profesion ) throws Exception{
        
        ClienteDTO clienteDTO = new ClienteDTO();
        ProfesionDTO profesionDTO = new ProfesionDTO();
        
        if(txtNumeroDocumento.isEmpty() ||  txtPrimerNombre.isEmpty() || txtPrimerApellido.isEmpty() || profesion == null || profesion.isEmpty() ){
            JOptionPane.showMessageDialog(vistaCliente, "Los datos como número de documento, primer nombre, primer apellido y profesión son obligatorios");
            throw new Exception("Los datos como número de documento, primer nombre, primer apellido y profesión son obligatorios");
        }
        
        clienteDTO.setNumeroDocumento(txtNumeroDocumento);
        clienteDTO.setPrimerNombre(txtPrimerNombre);
        clienteDTO.setSegundoNombre(txtSegundoNombre);
        clienteDTO.setPrimerApellido(txtPrimerApellido);
        clienteDTO.setSegundoApellido(txtSegundoApellido);
        clienteDTO.setDireccion(txtDireccion);
        clienteDTO.setTelefono(txtTelefono);
        clienteDTO.setNumeroCarnet(txtNumeroCarnet);
        profesionDTO.setNombre(profesion);
        clienteDTO.setProfesion(profesionDTO);

        clienteDAO.addCliente(clienteDTO);

        
    } 
    
    public List<ClienteDTO> consultarListaCliente(String numeroDocumento, String profesion) throws Exception{
        
        ClienteDTO clienteDTO  = null;
        List<ClienteDTO> listClientes  = new LinkedList<ClienteDTO>();
        
        if(numeroDocumento != null && numeroDocumento.length() == 0 ){
            numeroDocumento = null;
        }
        
        
        if(numeroDocumento != null &&  profesion == null){
            clienteDTO  = clienteDAO.getClientePorCedula(numeroDocumento);
            if(clienteDTO != null){
               listClientes.add(clienteDTO); 
            }
            
        }else if ( profesion != null && numeroDocumento == null ){
            listClientes  = clienteDAO.getClientesPorProfesion(profesion);
        }else{
            throw new Exception("Debe llenar un solo dato");
        }
        
        
        
        return listClientes;
        
    }
    
    public void consultarCliente(String numeroDocumento, String profesion) throws Exception{
        
        ClienteDTO clienteDTO  = null;
        int indice = 0;
        int totalRow = 0;
        Object [] object = new Object[7];
        List<ClienteDTO> listClientes  = new ArrayList<ClienteDTO>();
        
        totalRow = vistaCliente.getModeloTabClientes().getRowCount();
        
        for (totalRow= totalRow - 1; totalRow>= 0; totalRow--){
            vistaCliente.getModeloTabClientes().removeRow(totalRow);
        }
                
        
        if(numeroDocumento != null && numeroDocumento.length() == 0 ){
            numeroDocumento = null;
        }
        
        
        if(numeroDocumento != null &&  profesion == null){
            clienteDTO  = clienteDAO.getClientePorCedula(numeroDocumento);
            if(clienteDTO != null){
               listClientes.add(clienteDTO); 
            }
            
        }else if ( profesion != null && numeroDocumento == null ){
            listClientes  = clienteDAO.getClientesPorProfesion(profesion);
        }else{
            throw new Exception("Debe llenar un solo dato");
        }
        
        JOptionPane.showMessageDialog(vistaCliente, "El cliente se consultó exitosamente");
        
        if(!listClientes.isEmpty()){
             for(ClienteDTO cliente : listClientes){
                indice = 0;
                object[indice++] = cliente.getClienteId();
                object[indice++] = cliente.getNumeroDocumento();
                object[indice++] = cliente.getPrimerNombre() + " " + cliente.getPrimerApellido();
                object[indice++] = cliente.getDireccion();
                object[indice++] = cliente.getTelefono();
                object[indice++] = cliente.getNumeroCarnet();
                object[indice++] = cliente.getProfesion().getNombre();
                vistaCliente.getModeloTabClientes().addRow(object);
            }
        }
        
    }

}
