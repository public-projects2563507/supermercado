/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.com.supermercado.conexion;

import java.sql.Connection;
import java.sql.DriverManager;


/**
 *
 * @author Oscar Bonilla Sánchez
 */
public class Conexion {
    
     //conexion con php myadmin
    private static String driver = "com.mysql.jdbc.Driver";
    private static String connectString = "jdbc:mysql://209.126.3.87:3306/SUPERMERCADO_DB";
    private static String user = "USER_DB_SUPERMERCADO";
    private static String password = "USER_DB_SUPERMERCADO.2024.*";
    private static Connection connection;
    private static Conexion instance;
    
    
    private Conexion(){
        
    }
    
    public static Conexion getInstance() throws Exception{
        
        
        if(instance == null){
            instance = new Conexion();
        }
        
        return instance;
        
    }
        
    public void establecerConexion() throws Exception{
        
        Class.forName(driver);
        connection = DriverManager.getConnection(connectString, user , password);
    }
    
    public Connection getConnection(){
        
        return connection;
        
    }
   
    
}

